require 'rails_helper'

RSpec.feature 'Hello World', type: :feature do

  before do
    visit root_path
  end

  scenario 'responds 200' do
    expect(page.status_code).to eq 200
  end

  scenario 'renders page' do
    expect(page.body).to include('hello world')
  end

end
